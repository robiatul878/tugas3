import React, {Component} from 'react';
import {View, ScrollView} from 'react-native';
import {Header, Story, Pages} from './Index';

class App extends Component {
  render() {
    return (
      <View>
        <Header />
        <ScrollView>
          <Story />
          <Pages />
        </ScrollView>
      </View>
    );
  }
}

export default App;


