import React from 'react';
import { View, ScrollView, Image, } from 'react-native';

const Story = () => {
    return (
        <View>
            <ScrollView horizontal={true} 
            showsHorizontalScrollIndicator={false} style={{ height: 110, backgroundColor: 'white', paddingHorizontal: 15 }}>
                <Image source={require('../aset/aa.jpg')}
                    style={{ height: 75, width: 75, borderRadius: 60, marginTop: 20, marginHorizontal: 5 }} />
                <Image source={require('../aset/bb.jpg')}
                    style={{ height: 75, width: 75, borderRadius: 60, marginTop: 20, marginHorizontal: 5 }} />
                <Image source={require('../aset/cc.jpg')}
                    style={{ height: 75, width: 75, borderRadius: 60, marginTop: 20, marginHorizontal: 5 }} />
                <Image source={require('../aset/dd.jpg')}
                    style={{ height: 75, width: 75, borderRadius: 60, marginTop: 20, marginHorizontal: 5 }} />
                <Image source={require('../aset/ee.jpg')}
                    style={{ height: 75, width: 75, borderRadius: 60, marginTop: 20, marginHorizontal: 5 }} />
                <Image source={require('../aset/ff.jpg')}
                    style={{ height: 75, width: 75, borderRadius: 60, marginTop: 20, marginHorizontal: 5 }} />
            </ScrollView>
        </View>
    )
}

export default Story