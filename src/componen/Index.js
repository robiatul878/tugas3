import Header from './Header';
import Story from './Story';
import Pages from './Pages';

export { Header, Story, Pages}